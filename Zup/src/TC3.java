import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

class TC3 {

	@Test
	void test() {
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver_win32\\chromedriver.exe");   
		WebDriver driver = new ChromeDriver();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		
		acessarSite(driver);
		pesquisarProduto(driver,js);
		cep(driver,js);		
	}

	private void cep(WebDriver driver, JavascriptExecutor js) {
		String CEP = "83838-383";
		String enderecoCEPcorreto = "Francisco Chiaffitelli - Jardim Leonor - Campinas/SP";
		WebElement txtCEP = driver.findElement(By.name("zipcode"));
		txtCEP.click();
		txtCEP.sendKeys(CEP);
		
		WebElement btnCEP = driver.findElement(By.className("input__zipcode-button js-freight-button"));
		btnCEP.click();
		
		WebElement msgError = driver.findElement(By.className("unavailable__text-consult--red"));
		WebElement msgSuccess = driver.findElement(By.className("freight-product__freight-text-info js-freight-address"));
		
		if(msgError.isDisplayed()) {
			String msgerro = msgError.getText();
			if(msgerro.contains("Ocorreu um erro na pesquisa pelo CEP")) {
				System.out.println("Erro na pesquisa do CEP");
			}	
		} else {
			String msgsucesso = msgSuccess.getText();
			if(msgsucesso.contains("enderecoCEPcorreto")) {
				System.out.println("CEP Pesquisado com sucesso");
			}	
		}	
		
	}

	private void pesquisarProduto(WebDriver driver, JavascriptExecutor js) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		String produtoPesquisa = "iphone 7";
		WebElement inpHeaderSearch = driver.findElement(By.id("inpHeaderSearch"));
		inpHeaderSearch.click();
		inpHeaderSearch.sendKeys(produtoPesquisa);
		WebElement btnHeaderSearch = driver.findElement(By.id("btnHeaderSearch"));
		btnHeaderSearch.click();
		WebElement nomePesquisado = driver.findElement(By.id("main-title"));
		String valor = nomePesquisado.getText();
		String produto = "iphone 7".toUpperCase();
		if(valor.equals(produto)){
			System.out.println("Pesquisado o " + produto +".");
		}
		js.executeScript("document.querySelector('#product_218009200').click();");
		
		WebElement nomePesquisado2 = driver.findElement(By.xpath("/html/body/div[3]/div[5]/div[1]/div[3]/h1"));
		String valor23 = nomePesquisado2.getText().toUpperCase();
		System.out.println(valor23);
		if(valor23.contains(produto)){
			System.out.println("Pesquisado o " + produto +" clicando no produto");
		}
		
	}

	private void acessarSite(WebDriver driver) {
		driver.manage().window().maximize();
		driver.get("https://www.magazineluiza.com.br/");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}		
	}

}