import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

class TC2 {

	@Test
	void test() {
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver_win32\\chromedriver.exe");   
		WebDriver driver = new ChromeDriver();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		
		acessarSite(driver);
		pesquisarProduto(driver,js);
	}

	private void pesquisarProduto(WebDriver driver, JavascriptExecutor js) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		String produtoPesquisa = "XXXXXXXMRPIIIIIIII";
		WebElement inpHeaderSearch = driver.findElement(By.id("inpHeaderSearch"));
		inpHeaderSearch.click();
		inpHeaderSearch.sendKeys(produtoPesquisa);
		WebElement btnHeaderSearch = driver.findElement(By.id("btnHeaderSearch"));
		btnHeaderSearch.click();
		WebElement resultadoPesquisa = driver.findElement(By.className("nm-not-found-message2"));
		String valor = resultadoPesquisa.getText();
		if(valor.contains("Por favor, tente outra vez com termos menos específicos")) {
			System.out.println("Resultado Pesquisa: " + valor);
		}
			
	}

	private void acessarSite(WebDriver driver) {
		driver.manage().window().maximize();
		driver.get("https://www.magazineluiza.com.br/");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}		
	}

}