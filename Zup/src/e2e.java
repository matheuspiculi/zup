import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

class e2e {

	@Test
	void test() {
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver_win32\\chromedriver.exe");   
		WebDriver driver = new ChromeDriver();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		
		acessarSite(driver);
		pesquisarProduto(driver,js);
		//selecionarProduto(driver,js);
		//adicionarAoCarinho(driver,js);
		//carrinho(driver,js);
		
	}

	private void carrinho(WebDriver driver, JavascriptExecutor js, String garantiaExtendida, String valor23) {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		String headerCarrinhoBase = "Sacola";
		WebElement headerCarrinho = driver.findElement(By.className("BasketTable-header-product"));
		String valor = headerCarrinho.getText();
		System.out.println("carrinho, variavel 'valor' " + valor);
		if(valor.equals(headerCarrinhoBase)){
			System.out.println("A pagina est� no carrinho de compras");
		}
		
		WebElement nomeProduto = driver.findElement(By.className("BasketItem-product-info-title"));
		String valor2 = nomeProduto.getText().toUpperCase();
		System.out.println("carrinho, variavel 'valor2' " + valor2);
		
		WebElement adicionalProduto = driver.findElement(By.className("BasketItem-warranty-title"));
		String valor3 = adicionalProduto.getText().toUpperCase();
		System.out.println("carrinho, variavel 'valor3' " + valor3);
		if(valor3.contains(garantiaExtendida)) {
			System.out.println("Garantia extendida de 1 ano selecionada ao produto");
		}
		
		if(valor2.equals(valor23)) {
			System.out.println("Selecionado o produto: " + valor2);
		}
		
		
	}
	
	public void opcoesExtras(WebDriver driver, JavascriptExecutor js, String valor23) {
		String garantiaExtendida = null;
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		//Adicao Garantia Extendida - mais 1 ano de garantia
		WebElement checkbox = driver.findElement(By.id("one-year-warranty"));
		checkbox.click();
		garantiaExtendida = "1 ANO";
		js.executeScript("document.querySelector('.price-warranty__btn--continue.btn-buy-warranty').click();");
		
		carrinho(driver,js,garantiaExtendida,valor23);
	}

	private void adicionarAoCarinho(WebDriver driver, JavascriptExecutor js, String valor23) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		js.executeScript("document.querySelector('.js-add-cart-button').click();");		
		
		opcoesExtras(driver,js,valor23);
	}

	private void pesquisarProduto(WebDriver driver, JavascriptExecutor js) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		String produtoPesquisa = "iphone 7";
		WebElement inpHeaderSearch = driver.findElement(By.id("inpHeaderSearch"));
		inpHeaderSearch.click();
		inpHeaderSearch.sendKeys(produtoPesquisa);
		WebElement btnHeaderSearch = driver.findElement(By.id("btnHeaderSearch"));
		btnHeaderSearch.click();
		WebElement nomePesquisado = driver.findElement(By.id("main-title"));
		String valor = nomePesquisado.getText();
		String produto = "iphone 7".toUpperCase();
		if(valor.equals(produto)){
			System.out.println("Pesquisado o " + produto +".");
		}
		js.executeScript("document.querySelector('#product_218009200').click();");
		
		WebElement nomePesquisado2 = driver.findElement(By.xpath("/html/body/div[3]/div[5]/div[1]/div[3]/h1"));
		String valor23 = nomePesquisado2.getText().toUpperCase();
		System.out.println(valor23);
		if(valor23.contains(produto)){
			System.out.println("Pesquisado o " + produto +" clicando no produto");
		}
		
		adicionarAoCarinho(driver,js,valor23);
		
	}

	private void acessarSite(WebDriver driver) {
		driver.manage().window().maximize();
		driver.get("https://www.magazineluiza.com.br/");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}		
	}

}